package main

import (
	"encoding/json"
	"hash/crc32"
	"strings"

	"gitlab.com/kirafan/cri/unpacker/utils"
)

// ADV is one row in ADVList.json
type ADV struct {
	AdvID      uint32 `json:"m_AdvID"`
	Category   uint32 `json:"m_Category"`
	CueSheet   string `json:"m_CueSheet"`
	CRIVersion uint32 `json:"m_CRIVersion"`
}

func story(storyType string) []Job {
	jobs := make([]Job, 0)

	data, err := utils.Download("https://database.kirafan.cn/database/ADVList.json")
	if err != nil {
		panic(err)
	}
	var advs []ADV
	err = json.Unmarshal(data, &advs)
	if err != nil {
		panic(err)
	}
	for _, adv := range advs {
		if len(adv.CueSheet) == 0 || !strings.HasPrefix(adv.CueSheet, storyType) {
			continue
		}
		if crc32.ChecksumIEEE([]byte(adv.CueSheet))%uint32(*J) != uint32(*j) {
			continue
		}
		if *f || !exist(adv.CueSheet, adv.CRIVersion) {
			jobs = append(jobs, Job{
				Name:    adv.CueSheet,
				Version: adv.CRIVersion,
			})
		}
	}
	return jobs
}

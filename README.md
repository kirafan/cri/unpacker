# Kirara Fantasia CRI

CRIWARE unpacker for Kirara Fantasia

## Usage

```
go get -u ./...
go run main.go
```

## Terms

### ACB File

A UTF-table. Usually 20~50 KB, containing header and basic information.

Useful tables in it:

* **CueTable**
* **CueNameTable**
* **WaveformTable**
* **SequenceTable**
* **SynthTable** seems to be useless

**Version** is defined in each ACB file. Nowable the version can be one of 0x01300000 (1.30.0) and 0x01330000 (1.33.0) There is difference between two versions when unpacking.

### AWB File

An AFS-archive. From 100 KB to 10 MB, containing the audio data.

* files: length 151
  * cue_id
  * offset
  * size

## Unpack Procedure

1. Sort **CueNameTable** by CueIndex.

2. Know that one cue matches one or more waveform. This relation is defined:
  * As NumTracks in **SequenceTable** if ACB version is 1.30.0
  * As NumRelatedWaveforms in **CueTable** if ACB version is 1.33.0
  > In version 1.30.0, the length of **SequenceTable** is equal to **CueTable** so each sequence matches one cue. But in version 1.33.0 this relationship breaks. The length of **SequenceTable** is a larger number than **CueTable**, but also not equal to the length of **WaveformTable**. Thankfully, **CueTable** contains this information, making **SequenceTable** useless when unpacking however.

3. Find the correct AWB index by count the number of waveforms.

> ## Old Procedure (Wrong)
> 
> 1. Extract the UTF-table from the ACB file.
> 
> 2. Extract the AFS-archive from the AWB file.
> 
> 3. Get track list by:
> 
>    1. Construct a map by **CueNameTable**;
> 
>    2. Get ReferenceItems in **SynthTable** by ReferenceIndex in **CueTable**;
> 
>    3. Get MemoryAwbId or Id in **WaveformTable** by ReferenceItems in step 2;
> 
>       Here all MemoryAwbIds are 65535 and Id does not exist.
> 
>    4. Get EncodeType in **WaveformTable** by ReferenceItems in step 2;
> 
>       Here all EncodeTypes are 6.
> 
>    5. Get Streaming in **WaveformTable** by ReferenceItems in step 2;
> 
>       Here all Streamings are 1.
> 
>    6. Push into array.
> 
> 4. Save tracks by CueId in AFS-archive.
> 
> This old procedure is unquestionably wrong because the number of files AWB file contains is equal to the length of **WaveformTable**, but not **CueTable**.


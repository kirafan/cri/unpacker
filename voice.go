package main

import (
	"encoding/json"
	"hash/crc32"

	"gitlab.com/kirafan/cri/unpacker/utils"
)

// CRIFileVersion is one row in CRIFileVersion.json
type CRIFileVersion struct {
	FileName string        `json:"m_FileName"`
	FirstDL  utils.Boolean `json:"m_FirstDL"`
	Version  uint32        `json:"m_Version"`
	IsACB    utils.Boolean `json:"m_IsAcb"`
	IsAWB    utils.Boolean `json:"m_IsAwb"`
	IsACF    utils.Boolean `json:"m_IsAcf"`
	IsUSM    utils.Boolean `json:"m_IsUsm"`
}

func voice() []Job {
	jobs := make([]Job, 0)

	data, err := utils.Download("https://database.kirafan.cn/database/CRIFileVersion.json")
	if err != nil {
		panic(err)
	}
	var criFileVersions []CRIFileVersion
	err = json.Unmarshal(data, &criFileVersions)
	if err != nil {
		panic(err)
	}
	for _, criFileVersion := range criFileVersions {
		if !criFileVersion.IsACB || !criFileVersion.IsAWB {
			continue
		}
		if crc32.ChecksumIEEE([]byte(criFileVersion.FileName))%uint32(*J) != uint32(*j) {
			continue
		}
		if *f || !exist(criFileVersion.FileName, criFileVersion.Version) {
			jobs = append(jobs, Job{
				Name:    criFileVersion.FileName,
				Version: criFileVersion.Version,
			})
		}
	}
	return jobs
}

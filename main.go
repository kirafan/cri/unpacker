package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"gitlab.com/kirafan/cri/unpacker/afs"
	"gitlab.com/kirafan/cri/unpacker/hca"
	"gitlab.com/kirafan/cri/unpacker/utf"
	"gitlab.com/kirafan/cri/unpacker/utils"
	rvh "gitlab.com/kirafan/resource-version-hash"
)

type Job struct {
	Name    string
	Version uint32
}

var (
	f = flag.Bool("f", false, "force")
	j = flag.Int("j", 0, "job index")
	J = flag.Int("J", 1, "job number")
	t = flag.Int("t", 8, "thread number")
)

const host = "https://asset-krr-prd.star-api.com"

func handle(job Job) {
	name := job.Name
	version := job.Version
	fmt.Println(name, "...")
	defer func() {
		if err := recover(); err != nil {
			fmt.Println(name, err)
		} else {
			fmt.Println(name, "ok")
		}
	}()

	acb, err := utils.Download(fmt.Sprintf("%s/%s/CRI/%s.acb", host, rvh.Get(), name))
	if err != nil {
		log.Println(err)
		return
	}
	awb, err := utils.Download(fmt.Sprintf("%s/%s/CRI/%s.awb", host, rvh.Get(), name))
	if err != nil {
		log.Println(err)
		return
	}

	table := utf.NewUTFTable(acb)
	cueTable := table.Rows[0]["CueTable"].(*utf.UTFTable)
	cueNameTable := table.Rows[0]["CueNameTable"].(*utf.UTFTable)
	sequenceTable := table.Rows[0]["SequenceTable"].(*utf.UTFTable)
	waveformTable := table.Rows[0]["WaveformTable"].(*utf.UTFTable)
	archive := afs.NewAFSArchive(awb)

	cueNames := make(map[uint16]string)
	for _, cueNameRow := range cueNameTable.Rows {
		cueIndex := cueNameRow["CueIndex"].(uint16)
		cueName := cueNameRow["CueName"].(string)
		cueNames[cueIndex] = cueName
	}

	acbVersion := table.Rows[0]["Version"].(uint32)
	os.MkdirAll(fmt.Sprintf("cri/%s", name), 0755)
	if false {
		data, _ := json.Marshal(table)
		ioutil.WriteFile(fmt.Sprintf("cri/%s/table.json", name), data, 0644)
	}

	switch {
	case false:
		for i := range archive.Files {
			hca.Decode(fmt.Sprintf("cri/%s/_%d", name, i), archive.Files[i])
		}

	case acbVersion == 0x1300000:
		index := uint16(0)
		for i, sequenceRow := range sequenceTable.Rows {
			numTracks := sequenceRow["NumTracks"].(uint16)
			cueName := cueNames[uint16(i)]
			for j := uint16(0); j < numTracks; j++ {
				awbID := waveformTable.Rows[index]["StreamAwbId"].(uint16)
				hca.Decode(fmt.Sprintf("cri/%s/%s_%d", name, cueName, j), archive.Files[awbID])
				index++
			}
		}

	case acbVersion == 0x1330000:
		index := uint16(0)
		streaming := uint8(1)
		for i, cueRow := range cueTable.Rows {
			numTracks := cueRow["NumRelatedWaveforms"].(uint16)
			cueName := cueNames[uint16(i)]
			for j := uint16(0); j < numTracks; j++ {
				for waveformTable.Rows[index]["Streaming"].(uint8) != streaming {
					index++
				}
				awbID := waveformTable.Rows[index]["StreamAwbId"].(uint16)
				hca.Decode(fmt.Sprintf("cri/%s/%s_%d", name, cueName, j), archive.Files[awbID])
				index++
			}
		}

	default:
		log.Panicln("undefined ACB version", acbVersion, name)
	}

	ioutil.WriteFile(fmt.Sprintf("cri/%s/%d", name, version), []byte{}, 0644)
}

func exist(name string, version uint32) bool {
	_, err := os.Stat(fmt.Sprintf("cri/%s/%d", name, version))
	return err == nil || os.IsExist(err)
}

func main() {
	flag.Parse()

	jobChannel := make(chan Job)
	quit := make(chan bool)

	criTypes := flag.Args()
	jobs := make([]Job, 0)
	for _, criType := range criTypes {
		switch criType {
		case "voice":
			jobs = append(jobs, voice()...)
		case "main":
			jobs = append(jobs, story("mainstory")...)
		case "event":
			jobs = append(jobs, story("event")...)
		case "weapon":
			jobs = append(jobs, story("weaponevent")...)
		}
	}
	go func() {
		for _, job := range jobs {
			jobChannel <- job
		}
		close(jobChannel)
	}()
	for i := 0; i < *t; i++ {
		go func() {
			for job := range jobChannel {
				handle(job)
			}
			quit <- true
		}()
	}
	for i := 0; i < *t; i++ {
		<-quit
	}
}

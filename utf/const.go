package utf

const (
	columnStorageMask = uint8(0xF0)
	columnTypeMask    = uint8(0x0F)

	columnStorageConstant  = uint8(0x30)
	columnStorageConstant2 = uint8(0x70)
)

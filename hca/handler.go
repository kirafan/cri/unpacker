package hca

import (
	"io/ioutil"
	"log"

	"github.com/vazrupe/go-hca/hca"
)

// Decode decodes hca file to wav
func Decode(name string, data []byte) {
	h := hca.NewDecoder()
	h.CiphKey1 = 0x8706E529
	h.CiphKey2 = 0x00B6A392

	wav, ok := h.DecodeFromBytes(data)
	if !ok {
		log.Println("HCA decode error:", name)
		ioutil.WriteFile(name+".hca", data, 0644)
	} else {
		ioutil.WriteFile(name+".wav", wav, 0644)
	}
}
